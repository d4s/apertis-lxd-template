{
    "architecture": "${arch}",
    "creation_date": ${gendate},
    "properties": {
        "architecture": "${arch}",
        "description": "Apertis ${release} ${type} ${arch} ${date}.${build}",
        "name": "apertis_${release}-${arch}-${type}_${date}.${build}",
        "os": "apertis",
        "release": "${release}",
        "variant": "{$type}"
    },
    "templates": {
        "/etc/hostname": {
            "template": "hostname.tpl",
            "when": [
                "create",
                "copy"
            ]
        },
        "/etc/hosts": {
            "template": "hosts.tpl",
            "when": [
                "create",
                "copy"
            ]
        }
    }
}
