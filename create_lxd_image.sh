#!/bin/sh

set -eu

# Apertis options
LONGOPTS="arch:,baseurl:,release:,date:,build:,type:,board:,force,import"
SHORTOPTS="a:r:d:t:b:"

#LXC internal options
LONGOPTS="$LONGOPTS,name:,path:,rootfs:,mapped-uid:,mapped-gid:"
OPTS=$(getopt -o $SHORTOPTS -l $LONGOPTS -- "$@")

eval set -- "$OPTS"

architecture="amd64"
baseurl="images.apertis.org"
release="18.06"
type="minimal"
date="$(date +%Y%m%d)"
build="0"
board="uefi"
force=0
import=0

# Check options
while true; do
    case "$1" in
        -a|--arch)  architecture=$2; shift 2;;
        --baseurl)  baseurl=$2; shift 2;;
        -r|release) release=$2; shift 2;;
        -t|--type)  type=$2; shift 2;;
        -d|--date)  date=$2; shift 2;;
        --build)    build=$2; shift 2;;
        -b|--board) board=$2; shift 2;;
        --force)    force=1; shift;;
        --import)   import=1; shift;;
        *)          break;;
    esac
done


# Construct filenames to download
OSPACK=https://${baseurl}/daily/${release}/${date}.${build}/${architecture}/${type}/ospack_${release}-${architecture}-${type}_${date}.${build}.tar.gz

TARBALL="${OSPACK##*/}"

if [ ! -f "$TARBALL" -o $force -eq 1 ]; then
    [ -f "$TARBALL" ] && rm -f "$TARBALL"
    wget "$OSPACK"
fi

# Create metadata from variables
arch=$architecture
[ "$arch" == "amd64" ] && arch=x86_64
export arch
export gendate="$(date +%s)"

export baseurl
export release
export type
export date
export build
export board

envsubst < metadata/metadata.yaml.tpl > metadata/metadata.yaml

# Create the tarball with metadata
metafile=apertis_${release}-${type}-${architecture}-${board}_${date}.${build}-meta.tar.gz
tar -czf "$metafile" -C metadata metadata.yaml templates/

[ "$import" == "0" ] && exit 0

lxc image import "$metafile" "$TARBALL" --alias Apertis/${release}/${architecture}/${type}
